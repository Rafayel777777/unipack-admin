import { createAction, createReducer } from '../helpers';

/** Action types **/
const SET_EXAMPLE = 'SET_EXAMPLE';
const UPDATE_EXAMPLE = 'UPDATE_EXAMPLE';
const RESET_EXAMPLE = 'RESET_EXAMPLE';

/** Actions **/
export const setExample = createAction(SET_EXAMPLE);
export const updateExample = createAction(UPDATE_EXAMPLE);
export const resetExample = createAction(RESET_EXAMPLE);

const initial = {
    name: '100',
    age: '',
    isHuman: true,
};

/** Reducer **/
export const example = createReducer(initial, (state, { value }) => ({
    [SET_EXAMPLE]: value,
    [UPDATE_EXAMPLE]: { ...state, ...value },
    [RESET_EXAMPLE]: initial,
}));
