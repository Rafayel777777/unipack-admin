export function isDevelopmentStage() {
	return process.env.NODE_ENV === 'development';
}
